;; -*- lexical-binding: t -*-

(add-to-list 'load-path "~/.guix-profile/share/emacs/site-lisp")
(guix-emacs-autoload-packages)

(require 'org-tempo)

(setq initial-major-mode 'org-mode
      org-startup-folded t

      make-backup-files nil
      default-directory "~/"
      warning-minimum-level :emergency
      ring-bell-function 'ignore
      tool-bar-mode nil
      tooltip-mode nil
      font-lock-maximum-decoration t
      blink-cursor-mode t
      overflow-newline-into-fringe t
      global-hl-line-mode t

      ido-enable-flex-matching t
      icomplete-separator "\n"
      icomplete-hide-common-prefix nil
      icomplete-in-buffer t
      ido-default-file-method 'selected-window
      ido-default-buffer-method 'selected-window
      icomplete-matches-format nil)

(require 'ido)
(ido-mode 1)
(setf (nth 2 ido-decorations) "\n")
       
(require 'icomplete)
(icomplete-mode 1)
(fido-vertical-mode 1)

(setq-default message-log-max nil
 	      cursor-type 'hbar
 	      mode-line-format nil)

(customize-set-variable
 'display-buffer-base-action
 '((display-buffer-reuse-window display-buffer-same-window
    display-buffer-in-previous-window
    display-buffer-use-some-window)))

(setq doc-view-resolution 240)
(setq flyspell-issue-message-flag nil)

(let ((ligatures `((?-  . ,(regexp-opt '("-|" "-~" "---" "-<<" "-<" "--" "->" "->>" "-->")))
                   (?/  . ,(regexp-opt '("/**" "/*" "///" "/=" "/==" "/>" "//")))
                   (?*  . ,(regexp-opt '("*>" "***" "*/")))
                   (?<  . ,(regexp-opt '("<-" "<<-" "<=>" "<=" "<|" "<||" "<|||::=" "<|>" "<:" "<>" "<-<"
                                         "<<<" "<==" "<<=" "<=<" "<==>" "<-|" "<<" "<~>" "<=|" "<~~" "<~"
                                         "<$>" "<$" "<+>" "<+" "</>" "</" "<*" "<*>" "<->" "<!--")))
                   (?:  . ,(regexp-opt '(":>" ":<" ":::" "::" ":?" ":?>" ":=")))
                   (?=  . ,(regexp-opt '("=>>" "==>" "=/=" "=!=" "=>" "===" "=:=" "==")))
                   (?!  . ,(regexp-opt '("!==" "!!" "!=")))
                   (?>  . ,(regexp-opt '(">]" ">:" ">>-" ">>=" ">=>" ">>>" ">-" ">=")))
                   (?&  . ,(regexp-opt '("&&&" "&&")))
                   (?|  . ,(regexp-opt '("|||>" "||>" "|>" "|]" "|}" "|=>" "|->" "|=" "||-" "|-" "||=" "||")))
                   (?.  . ,(regexp-opt '(".." ".?" ".=" ".-" "..<" "...")))
                   (?+  . ,(regexp-opt '("+++" "+>" "++")))
                   (?\[ . ,(regexp-opt '("[||]" "[<" "[|")))
                   (?\{ . ,(regexp-opt '("{|")))
                   (?\? . ,(regexp-opt '("??" "?." "?=" "?:")))
                   (?#  . ,(regexp-opt '("####" "###" "#[" "#{" "#=" "#!" "#:" "#_(" "#_" "#?" "#(" "##")))
                   (?\; . ,(regexp-opt '(";;")))
                   (?_  . ,(regexp-opt '("_|_" "__")))
                   (?\\ . ,(regexp-opt '("\\" "\\/")))
                   (?~  . ,(regexp-opt '("~~" "~~>" "~>" "~=" "~-" "~@")))
                   (?$  . ,(regexp-opt '("$>")))
                   (?^  . ,(regexp-opt '("^=")))
                   (?\] . ,(regexp-opt '("]#"))))))
  (dolist (char-regexp ligatures)
    (set-char-table-range composition-function-table (car char-regexp)
                          `([,(cdr char-regexp) 0 font-shape-gstring]))))


(require 'org)
(setq org-export-with-broken-links t
      org-pretty-entities t
      org-pretty-entities-include-sub-superscripts t
      org-startup-indented t
      org-startup-with-inline-images t
      org-image-actual-width '(300)
      org-src-tab-acts-natively t
      ;org-enable-notifications t
      org-start-notification-daemon-on-startup t)

(setq org-agenda-files (quote ("~/")))
(require 'shell)

(setq org-roam-directory "~/"
      org-roam-graph-executable "dot"
      org-roam-graph-viewer nil
      org-roam-v2-ack t
      org-hide-block-startup t)

(setq dired-guess-shell-alist-user
      '(("\\.\\(?:xcf\\)\\'" "gimp")
        ("\\.\\(?:mp4\\|mkv\\|avi\\|flv\\|ogv\\|webm\\|mp3\\|flac\\|opus\\|ogg\\)\\(?:\\.part\\)?\\'" "mpv")))

(set-face-attribute 'default nil
                    :family "Unifont"
                    :height 120
                    :weight 'normal
                    :width 'normal)

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-safe-themes
   '("9096b5a82bde4ba8a327040a97d5efee124b60c540633d643acc6e2c47666dd2" default))
 '(org-agenda-files '("~/Desktop/work/geschichte.org")))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
(load-theme 'soot)
