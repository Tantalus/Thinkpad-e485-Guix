;(server-start)
(menu-bar-mode -1)
(tool-bar-mode -1)
(auto-save-mode -1)
;(scroll-bar-mode -1)
(setq 
;fringe-mode 0
      inhibit-startup-screen t
      initial-scratch-message ""
      visible-bell t
      backup-inhibited t
      display-time-format "%H:%M")
(defalias 'yes-or-no-p 'y-or-n-p) 
(global-prettify-symbols-mode 1)
(prefer-coding-system 'utf-8)
(set-keyboard-coding-system 'utf-8)
